home.description:
    other: "KDE is 25 years old! Come celebrate with us, attend our talks, join
    our offline meetups, check out the history of the project — and play with
    some of historic landmark desktops and software, and get some really cool merch!"

home.activities:
    other: "Activities"
home.activities.description:
    other: "<p>Join our fireside talks and meet other KDE friends and
    enthusiasts.</p>
    
    <p><b>NOTE:</b> Links to the actual talks will appear here shortly before
    each chat starts.</p>"

home.activities.fireside:
    other: "Fireside Chats"
home.activities.fireside.1:
    other: "October 14, at 17:00 UTC: KDE e.V. Board"
home.activities.fireside.1.description:
    other: "Aleix, Lydia, Eike, Adriaan and Neofytos are the KDE volunteers
    that run [KDE e.V.](https://ev.kde.org/), the foundation that supports the
    KDE Community in all organizational, financial and legal matters. In this
    fireside chat we sit down with all five of them to talk about KDE's history,
    achievements and where they think KDE is headed. We will also dive into what
    it means to support the KDE Community for almost 25 years."
home.activities.fireside.2:
    other: "October 15, at 18:00 UTC: Jeri Ellsworth"
home.activities.fireside.2.description:
    other: "Jeri Ellsworth, self-taught electronics engineer, race driver, and
    maker and inventor supreme, will give us a glimpse into her latest project:
    [Tilt 5](https://www.tiltfive.com/), an augmented reality system for
    collaborative tabletop gaming. Yes, it is awesome as it sounds."
home.activities.fireside.3:
    other: "October 23, at 15:00 UTC: Massimo Stella"
home.activities.fireside.3.description:
    other: "Massimo Stella of Kdenlive will talk about how KDE's video editor
    came to be, where it is now and what we can look forward to in the future of
    open source film-making."
home.activities.fireside.4:
    other: "October 25, at 17:00 UTC: Matthias Ettrich"
home.activities.fireside.4.description:
    other: "25 years ago Matthias Ettrich sent a now
    [famous email](https://groups.google.com/g/de.comp.os.linux.misc/c/SDbiV3Iat_s/m/zv_D_2ctS8sJ?pli=1)
    to the de.comp.os.linux.misc news group. In that message, Matthias asked
    developers to join him in building a Kool Desktop Environment (and a set of
    apps to match), all geared towards end users. We will chat with Matthias
    about how KDE started, dig into the details and fascinating anecdotes that
    shaped the project and our community in its early days, and discover his
    perspective on what has become of his vision since then."
home.activities.fireside.registration:
    other: Register here
home.activities.fireside.ifregistered:
    other: "The room opens shortly before the beginning of the talk."
home.activities.fireside.access:
    other: Access room
    
home.activities.meetups:
    other: Meetups
home.activities.meetups.description:
    other: "Let's celebrate KDE's 25th Anniversary together! Join the
    meetups and get together with fellow Community members, make friends, and
    explore similar interests. Events are happening, both online and in-person! 
    You can join the in-person events in Berlin, Malaga, Barcelona, Nurnberg,
    or Valencia or host your own event in your city and meet for coffee or
    drinks."
home.activities.meetups.more:
    other: "Find a meetup near you!"

home.activities.AMA:
    other: Reddit AMA
home.activities.AMA.description:
    other: "Nate Graham (developer and author of _This Week in KDE_), Aleix Pol
    (President of KDE), and Lydia Pintscher (Vice President) will be answering
    all your questions on [Reddit](https://reddit.com/r/kde) live on Sunday,
    October 17th at 16:00 UTC. Don't miss it!"
home.activities.AMA.end:
    other: "But you can still visit [KDE's subreddit](https://reddit.com/r/kde)
    and receive up to date news, interact with KDE contributors, and participate
    in lively discussions!"
home.activities.AMA.button:
    other: "Visit r/kde"
    
home.Plasma:
    other: "Plasma - 25th Anniversary Edition"
home.Plasma.description:
    other: "<p>In time to celebrate KDE's birthday, Plasma renews its looks
    performance and speed. You get a new wallpaper, an updated theme, extra
    speed, increased reliability and new features.</p>
    
    <p>Find out about all the details that make the new Plasma so deserving of
    this 25th Anniversary on <a href='https://kde.org/announcements/plasma/5/5.23.0/'>the
    release's announcement page</a>.</p>"

'home.plasma.see-announcement':
    other: "See Announcement"

home.history:
    other: "The History of KDE"
home.history. description:
    other: "The history of KDE is a fascinating one. In this section, apart from
    learning about how things started (and how they are going), you will be able
    to see what things looked like as they evolved and actually test-drive some
    of KDE's most iconic desktops through the ages."

home.history.VMs:
    other: "Historic Desktops"
home.history.VMs.description:
    other: "Download and try historic versions of Plasma and KDE on easy to run 
    virtual machines."
home.history.VMs.kde1:
    other: "KDE 1: 1998 - 2000. Coming soon"
home.history.VMs.kde2:
    other: "KDE 2: 2000 - 2002. Coming soon"
home.history.VMs.kde3:
    other: "KDE 3: 2002 - 2008"
home.history.VMs.kde4:
    other: "Plasma 4: 2008 - 2014"
home.history.VMs.kde5:
    other: "Plasma 5: 2014 - 2017"
home.history.VMs.instructions:
    other: Instructions
home.history.VMs.instructions.description:
    other: "<ol>
    <li>Download and Install <a href='https://www.virtualbox.org/wiki/Downloads'>VirtualBox</a>
    on your computer</li>
    <li>Right-click on the icons below and choose <i>Save link as...</i> to
    download the VBox images to your computer</li>
    <li>Navigate to where you downloaded the images and click on them</li>
    <li>Choose to open them with VirtualBox</li>
    </ol>"    
home.history.VMs.credentials:
    other: "Credentials"
home.history.VMs.credentials.description:
    other: "<ul>
    <li><b>User name:</b> kde</li>
    <li><b>User Password:</b> kdeproject</li>
    <li><b>Root Password:</b> kdeproject</li>
    </ul>
    <p>VB images very kindly created by Hélio Castro.</p>"
    
home.history.timeline:
    other: "The KDE Timeline"
home.history.timeline.description:
    other: "<p>As is well known, the Universe was born 14 billion years ago. Then
    nothing much happened until KDE popped into existence on the 14th of October
    of 1996... That's when stuff really kicked off.</p>
    
    <p>Delve deep into KDE's history at our updated KDE Timeline.</p>"
home.history.timeline.link:
    other: "Visit the timeline"
    
    
home.history.gallery:
    other: Gallery
home.history.gallery.description:
    other: "Check out what things were like across the 25 years of KDE's existence."
home.history.gallery.CTA:
    other: "Have you got pictures or videos of the KDE Community
    you would like to show the world? <a href='mailto:press@kde.org'>Write to
    us!</a> We'd love to display your contribution in our gallery!"
home.history.gallery.biggerimage:
    other: "Right click on an image to see a bigger version."
    
home.history.gallery.1997kdeone:
    other: "'KDE One', probably the first KDE event. Held in Arnsberg, Germany in 1997"
home.history.gallery.1999kdetwo:
    other: "'KDE Two' was held in Erlangen, Germany, in 1999"
home.history.gallery.1999konqi:
    other: "Konqi made his debut in 1999"
home.history.gallery.2005wikipedia:
    other: "Jimmy Wales, founder of Wikipedia, and Matthias Ettrich, founder of KDE, in 2005"
home.history.gallery.2007amarokdevsprint:
    other: "Amarok Dev Sprint 2007"
home.history.gallery.2008KDE4:
    other: "Initial release of KDE 4 on Kubuntu - 2008. Thanks to Manuel Alberto Ascencio Ramirez."
home.history.gallery.2010akademy:
    other: "Akademy 2010 in Bilbao, Spain. [Source](https://www.flickr.com/photos/palazio/4595340538/). Thanks Gorka Palazio."
home.history.gallery.2015plasma-sprint:
    other: "Plasma Sprint 2015"
home.history.gallery.2015conf:
    other: "conf.kde.in 2015"
home.history.gallery.2015lakademy:
    other: "LAkademy 2015"
home.history.gallery.2016qtcon:
    other: "QtCon 2016 in Berlin, Germany."
home.history.gallery.202125thanniversary:
    other: "KDE's 25th Anniversary"

home.history.inthewild:
    other: KDE in the Wild
home.history.inthewild.description:
    other: "KDE's desktop and its applications have popped up in quite a few
    weird places over the years. From the _Hadron Collider_ to _Heroes_, KDE has
    made its way into all sorts of industries, organizations, films and TV shows.
    Here are just a few of KDE's most notable star moments:"
home.history.inthewild.CTA:
    other: "Have you seen other instances of KDE running in an interesting place
    or on a TV show or in a movie? <a href='mailto:press@kde.org'>Let us know</a>!
    If possible, send us a pic too, so we can share it with the rest of the world."

home.history.inthewild.ligo:
    other: "2018 - KDE 4 at [LIGO Hanford Observatory](https://www.ligo.caltech.edu/WA)
    (big screen). [Source](https://twitter.com/LIGOWA/status/1068552370013913089).
    Thanks Tóth Bálint."
    
home.history.inthewild.nasa:
    other: "2019 - The first images from Mars sent by NASA's InSight lander
    where displayed to the world using KDE 4. Image courtesy of NASA."
    
home.history.inthewild.sincrotron:
    other: "2019 - In the Alba Sincrotron, located near Barcelon, Spain, they
    shoot electrons along an accelerator to produce light for studies in
    different fields. Photo by Sergi Blanch-Torné. [Source](https://dot.kde.org/2019/07/19/powered-plasma-alba-synchrotron-barcelona-spain)."
    
home.history.inthewild.cern:
    other: "2014 - Plasma is used extensively throughout CERN, the place where
    the World Wide Web and the Large Hadron Collider were born. In this case, we
    see KDE 4 being used at a control station for a particle accelerator at
    Adlershof Technology Park, Germany. Photo courtesy of Thomas Weissel."

home.history.inthewild.nerc:
    other: "2021 - NERC's Space Geodesy Facility fires lasers at satellites to
    check their position to a high degree of precision. Scientists use a wide
    range of KDE software products on their wrkstations, including Plasma and
    Konsole. [Source](https://youtu.be/vdvIY0CJaXw)."

    
home.history.inthewild.kerala:
    other: "2017 - KDE's software, including the Plasma desktop, is used
    extensively throughout Kerala's educational system and is preinstalled on
    laptops pupils use. [Source](https://kite.kerala.gov.in/KITE/index.php/welcome/gallery_view/15)."

home.history.inthewild.24:
    other: "2003 - The good guys (as long as you consider CIA operatives \"good\")
    got to use KDE 3.0 in _24_ and it helped them to correctly identify who is
    probably the busiest spy in the agency, one Mr Jack Bauer, as he entered
    the HQ in Season 3, Episode 1. Source: Netflix. Thanks Eike Hein."
home.history.inthewild.heroes:
    other: "2006 - The wildly popular series _Heroes_ showed a character using
    KDE 3 apps and themes (Kicker, KMenu applet, Kopete, KMix, Keramik) in an
    episode that featured an intense chat session. [Source and more pics](http://www.eikehein.com/kde/heroes/).
    Thanks Eike Hein."
home.history.inthewild.dexter:
    other: "2006 - KDE 3 pops up again in the also popular, albeit darker,
    _Dexter_ in season 1, episode 8, running on the laptop of a dodgy
    psychiatrist. Source: Amazon Prime. Thanks Aaron Seigo." 
home.history.inthewild.gravity:
    other: "2013 - Alfonso Cuarón's smash hit movie _Gravity_, starring Sandra
    Bullock and George Clooney, may have had wonky physics, but its visual
    production was impeccable, thanks, in part, to Plasma 4, seen here running the
    software used by the visual effects artists. [Source and more pics](https://blogs.kde.org/2014/02/09/kde-plasma-movies).
    Thanks Eike Hein."


    

home.support:
    other: Support KDE!
home.support.description:
    other: "<p>Like many Free Software projects, at KDE we rely on volunteers and
    donations to carry out our work. Volunteers develop, contribute media,
    translate, promote, and help implement KDE software. Donations help us with
    all the operational costs of running a non-profit with thousands of members.</p>
    
    <p>You can find out how volunteers work together and monetary resources
    are allocated by taking a look at our <a href='https://ev.kde.org/reports/'>yearly reports</a>.</p>
    
    <p>You can contribute too! Here are some ways you can help us:</p>"
    
home.support.25425:
    other: "\"25 for 25\" Fundraising Campaign"
home.support.25425.description:
    other: "For our 25th Anniversary, we are running a special _25 for 25_ 
    fundraising campaign! Show your ❤️‍for KDE by donating $ 25 or € 25!" 
home.support.25425.button:
    other: "Donate 25 for 25"
    
home.support.donations:
    other: Donations
home.support.donations.description:
    other: "You can <a href='https://kde.org/community/donations/'>make a
    donation of the quantity of your choice</a> or become a
    <a href='https://relate.kde.org/'>KDE Supporting Member</a> and donate a
    yearly amount to KDE. Every bit helps!"
home.support.donations.button:
    other: Donate

home.support.contribute.kontributor:
    other: "Over the years more than<br><strong>7000 Kontributors</strong> joined"
home.support.contribute.learn:
    other: "Learn how to get involved"
home.support.contribute:
    other: "25 Ways to Contribute"
home.support.contribute.description:
    other: "<p>Becoming a KDE volunteer keeps the Community and our
    software alive. To celebrate KDE's 25th anniversary, we have compiled a list
    of 25 things you can do for KDE and Nate Graham has turned the list into
    <a href='https://pointieststick.com/2021/10/13/25-ways-you-can-contribute-to-kde/'>an
    entertaining and informative article</a> with tons of links to resources.</p>"
home.support.contribute.list:
    other: "<p>This is a brief summary:</p>
    
    <ol>
    <li>Application development</li>
    <li>Web development</li>
    <li>Software testing</li>
    <li><a href='https://bugs.kde.org'>Bug reporting</a></li>
    <li>Bug triaging</li>
    <li>Sending code fixes and new features</li>
    <li>Enabling <I>User Feedback</I> for Plasma on <I>System Settings</I></li>
    <li><a href='https://community.kde.org/Get_Involved/translation'>Translating</a></li>
    <li><a href='https://community.kde.org/Get_Involved/design'>Visual designing</a></li>
    <li><a href='https://t.me/kdeusability'>User experience (UX) designing</a></li>
    <li><a href='https://community.kde.org/Promo'>Promoting and doing marketing</a></li>
    <li>Working on tasks on <a href='https://phabricator.kde.org'>KDE's Phabricator</a></li>
    <li>Producing and editing videos for KDE's social media accounts</li>
    <li>Writing and updating documentation and manuals</li>
    <li>Writing <a href='https://community.kde.org/Main_Page'>wiki articles</a></li>
    <li>Organizing events</li>
    <li>Participating in events</li>
    <li>Answering users' questions on the <a href='https://forum.kde.org'>forum</a> or <a href='https://www.reddit.com/r/kde/'>Reddit</a></li>
    <li>Promoting KDE in your local community/school/business/family</li>
    <li>Evangelizing about KDE on the Internet</li>
    <li>Following, sharing and interacting with posts on KDE's social media:
        <ul>
            <li><a href='https://www.facebook.com/kde'>Facebook</a></li>
            <li><a href='https://www.linkedin.com/company/kde/'>LinkedIn</a> </li>
            <li><a href='https://twitter.com/kdecommunity'>KDE Community Twitter</a></li>
            <li><a href='https://twitter.com/Akademy'>Akademy Twitter</a></li>
            <li><a href='https://www.instagram.com/kdecommunity/'>Instagram</a></li>
            <li><a href='https://tube.kockatoo.org/a/kde_community/'>PeerTube</a></li>
            <li><a href='https://www.youtube.com/c/KdeOrg/videos'>YouTube</a></li>
            <li><a href='https://vk.com/kde_ru'>VK</a></li>
            <li><a href='https://joindiaspora.com/u/kdecommunity'>Diaspora</a></li>
            <li><a href='https://mastodon.technology/@kde'>Mastodon</a></li>
        </ul>
    <li>Sharing links pointing to KDE websites and social media on other platforms</li>
    <li>Writing posts or making videos about KDE for your personal blog post, social media and YouTube/PeerTube channels</li>
    <li>Using <a href='https://apps.kde.org'>KDE software</a></li>
    <li>Packaging KDE software for your distro/app store</li>
    </ol>"

home.support.companies:
    other: "Companies"
home.support.companies.description:
    other: "Are you a manager in a company that finds our work useful? Let's
    develop better code together! Many companies encourage their developers to
    contribute to KDE's software. You can too."
home.support.companies.button:
    other: "Get in touch"
home.support.companies.patron:
    other: "You can also sponsor our activities or, better yet, become
    <a href='https://ev.kde.org/getinvolved/supporting-members/'>a KDE
    patron</a>!"
    
home.merch:
    other: "Merch!"
home.merch.description:
    other: "Celebrate the occasion with KDE 25th Anniversary T-shirts and
    stickers! Remember: Freewear donates to KDE with every purchase."
home.merch.tfitted:
    other: "Fitted T-shirt"
home.merch.tnfitted:
    other: "Non-fitted T-shirt"
home.merch.stickers:
    other: "Stickers"
home.merch.button:
    other: Visit Freewear
